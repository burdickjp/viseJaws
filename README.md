# Vise jaws for milling machine vises

## About

## Design
The part was designed and CAM'd in [Autodesk Fusion 360](https://www.autodesk.com/products/fusion-360/overview). Design files are included in this repository. [Design files are also available on a360]().

## Stock

## Tool table
| position | description                                | stick out |     tool holder |
|---------:|--------------------------------------------|----------:|-----------------|
|        1 | [[Haimer 3D-Taster NG]]                    |        27 | [tormach 33048] |
|        4 | [[Niagara N47734]] dia:8 3FL flat carb TiN |        32 |          [ER16] |
|        7 | [[YG-1 0881KCN]] dia:8.8 2FL drill HSS Tin |        81 |          [ER32] |
|        9 | [[YG-1 D4107]] dia:8.4 2FL drill HSS TiN   |        37 |          [ER16] |

## Setups and operations

### Setup 1
Back and sides  
Use [3 inch vise](https://littlemachineshop.com/products/product_view.php?ProductID=2356&category=). 

| G54 | description        |
|-----|--------------------|
| X0  | left face of stock |
| Y0  | face of fixed jaw  |
| Z0  | top of stock       |

#### Order of operations
| Operation         | Description         |
|-------------------|---------------------|
| back_01_face      | finish back         |
| back_02_2dcontour | finish sides        |

### Setup 2
Bottom
Use [4 inch vise](https://glacern.com/gpv_412).  

| G55 |                   |
|-----|-------------------|
| X0  | left face of part |
| Y0  | face of fixed jaw |
| Z0  | top of stock      |

#### Order of operations
| Operation      | Description   |
|----------------|---------------|
| bottom_01_face | finish bottom |

### Setup 3
Front
Use a [4 inch vise](https://glacern.com/gpv_412).  

| G56 |                   |
|-----|-------------------|
| X0  | left face of part |
| Y0  | face of fixed jaw |
| Z0  | top of stock      |

#### Order of operations
| Operation               | Description                              |
|-------------------------|------------------------------------------|
| front_01_drill          | drill holes                              |
| front_02_pocketCircular | rough, finish counterbore, through hole  |

## License
This repository is licensed under a [CERN Open Hardware License](https://www.ohwr.org/projects/cernohl/wiki).

[Tormach 33048]: https://www.tormach.com/store/index.php?app=ecom&ns=prodshow&ref=33048&portrelay=1
[ER32]: https://www.tormach.com/store/index.php?app=ecom&ns=prodshow&ref=33266&portrelay=1
[ER16]: https://www.tormach.com/store/index.php?app=ecom&ns=prodshow&ref=31831&portrelay=1
[Niagara N47734]: https://amzn.com/B003CP0WNU
[Haimer 3D-Taster NG]: https://www.haimer-usa.com/products/measuring-instruments/sensors/3d-sensor/3d-sensor-new-generation/3d-sensor-new-generation.html
[YG-1 0881KCN]: https://amzn.com/B009NOAYTG
[YG-1 D4107]: https://amzn.com/B009NNCIW8
